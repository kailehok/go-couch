package main

import (
	"context"
	"fmt"



	"github.com/gorilla/mux"
	"github.com/go-kivik/kivik"

	// "github.com/flimzy/kivik" // Stable version of Kivik

	"encoding/json"

	"net/http"

	_ "github.com/go-kivik/couchdb" // The CouchDB driver
)

//createAsset creates type
func createAsset(w http.ResponseWriter, r *http.Request) {
	fmt.Println("create")
	

	var ast Asset
	err := json.NewDecoder(r.Body).Decode(&ast)
	if err!=nil{
		panic(err)
	}
	fmt.Println(ast.Name)

	doc:= struct{
		DocType string `json:"docType"`
		Name string `json:"name"`
		AssetType string `json:"assetType"` 
		
		Attributes []NameValue `json:"attributes"`


	}{"Asset",ast.Name,ast.AssetType,ast.Attributes}

	// doc := map[string]interface{}{

		

	// 	"docType": "Type",
	// 	"name":    typeOf.Name,
	// }
	docMap,err:=ToJSONCompatibleMap(doc)
	if err!=nil{
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	fmt.Println(ast.Name)

	rev, err := db.Put(context.TODO(),"Asset"+ast.Name, docMap)
	if err != nil {
		fmt.Println("Error")

		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusCreated, doc)
	fmt.Printf("Type document is inserted with revision %s \n", rev)

	return

}
//updateAsset creates type
func updateAsset(w http.ResponseWriter, r *http.Request) {
	fmt.Println("update")

	params:=mux.Vars(r)
	id:=params["id"]

	row:=db.Get(context.TODO(),"Asset"+id)
	if row.Rev==""{
	fmt.Println("Error")

		respondJSON(w, http.StatusNotFound,"Incorrect id ("+id+") provided!")
		return
	}
	

	var ast Asset
	_ = json.NewDecoder(r.Body).Decode(&ast)

	var doc map[string]interface{}

	if id=="Asset"+ ast.Name{
		doc= map[string]interface{}{
			"_rev":row.Rev,
			"doctType": "Type",
			"name": ast.Name,
			"assetType": ast.AssetType,
			"attributes": ast.Attributes,

		}

	}else { 
		doc= map[string]interface{}{
			
			"doctType": "Type",
			"name": ast.Name,
			"assetType": ast.AssetType,
			"attributes": ast.Attributes,


		}
	}





	fmt.Println(ast.Name)
	rev,err := db.Delete(context.TODO(),"Asset"+id,row.Rev)
	if err!=nil{
		fmt.Println("del")
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	rev, err = db.Put(context.TODO(),"Asset"+ast.Name, doc)
	if err != nil {
		fmt.Println("Error")

		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondJSON(w, http.StatusCreated, doc)
	fmt.Printf("Type document is inserted with revision %s \n", rev)

	return

}

//deleteAsset creates type
func deleteAsset(w http.ResponseWriter, r *http.Request) {
	fmt.Println("update")

	params:=mux.Vars(r)
	id:=params["id"]

	row:=db.Get(context.TODO(),"Asset"+id)
	if row.Rev==""{
	fmt.Println("Error")

		respondJSON(w, http.StatusNotFound,"Incorrect id ("+id+") provided!")
		return
	}
	_,err := db.Delete(context.TODO(),"Asset"+id,row.Rev)
	if err!=nil{
		fmt.Println("del")
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusOK,"Sucessfully deleted!")
	return
}

//readAsset creates type
func readAsset(w http.ResponseWriter, r *http.Request){


	rows,err:=db.Query(context.TODO(), "type", "asset",kivik.Options{"include_docs": true})
    if err != nil {
        panic(err)
    }

    fmt.Println("i dont see")
    test:=struct{
        ID string `json:"_id"`
        DocType string `json:"docType"`
		Name string `json:"name"`
		Attributes []NameValue `json:"attributes"`
    }{}
    results := []interface{}{}
    for rows.Next() {
       
        if err := rows.ScanDoc(&test); err != nil {
            panic(err)
        }
       results=append(results,test)
       fmt.Println(results)
       }
   
    if err!=nil{
        panic(err)
    }

    // doc,_:=ToJSONCompatibleMap(results)
	fmt.Println(results)
	respondJSON(w, http.StatusCreated, results)

}
