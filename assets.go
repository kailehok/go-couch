package main


//Attrs structure
type Attrs struct{
	Name string `json:"name"`
	Type string `json:"type"`
	Mandatory bool `json:"mandatory"`
}

//Type struct
type Type struct{
	DocType string `json:"docType"`
	Name string `json:"name"`
	
}

//AssetType structure
type AssetType struct{
	DocType string `json:"docType"`
	Name string `json:"name"`
	Attributes []Attrs `json:"attributes"`

}

//NameValue structure
type NameValue struct{
	Name string `json:"name"`
	Value interface{} `json:"value"`
}

//Asset structure
type Asset struct{
	DocType string `json:"docType"`
	Name string `json:"name"`
	AssetType string `json:"assetType"`
	Attributes []NameValue `json:"attributes"`
}
