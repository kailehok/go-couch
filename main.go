package main

import (
	"context"
	"fmt"
	"net/http"

	_ "github.com/go-kivik/couchdb" // The CouchDB driver
	"github.com/go-kivik/kivik"     // Development version of Kivik
	"github.com/gorilla/mux"
)

//db database
var db *kivik.DB

func main() {

	client, err := kivik.New("couch", "http://localhost:5984/")
	if err != nil {
		panic(err)
	}

	db = client.DB(context.TODO(), "animals")
	if err != nil {
		panic(err)
	}

	fmt.Println("i see")

	// doc := map[string]interface{}{

	//     "feet":     4,
	//     "greeting": "moo",
	// }

	// id,rev, err := db.CreateDoc(context.TODO(), doc)
	// if err != nil {
	//     panic(err)
	// }
	// fmt.Printf("Cow inserted with revision %s %s\n",id, rev)

	router := mux.NewRouter()
	fmt.Println("outside")
	router.HandleFunc("/type", createType).Methods("POST")
	router.HandleFunc("/type/{id}", updateType).Methods("PUT")
	router.HandleFunc("/type/{id}", deleteType).Methods("DELETE")
	router.HandleFunc("/type", readType).Methods("GET")
	router.HandleFunc("/assettype", createAssetType).Methods("POST")
	router.HandleFunc("/assettype/{id}", updateAssetType).Methods("PUT")
	router.HandleFunc("/assettype/{id}", deleteAssetType).Methods("DELETE")
    router.HandleFunc("/assettype", readAssetType).Methods("GET")
    router.HandleFunc("/asset", createAsset).Methods("POST")
	router.HandleFunc("/asset/{id}", updateAsset).Methods("PUT")
	router.HandleFunc("/asset/{id}", deleteAsset).Methods("DELETE")
	router.HandleFunc("/asset", readAsset).Methods("GET")

	err = http.ListenAndServe(":8080", router)
	if err != nil {
		fmt.Println("inside")
		panic(err)
	}

}
