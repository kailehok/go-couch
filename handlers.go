package main

import (
	"context"
	"fmt"



	"github.com/gorilla/mux"
	"github.com/go-kivik/kivik"

	// "github.com/flimzy/kivik" // Stable version of Kivik

	"encoding/json"

	"net/http"

	_ "github.com/go-kivik/couchdb" // The CouchDB driver
)




//createType creates type
func createType(w http.ResponseWriter, r *http.Request) {
	fmt.Println("create")
	

	var typeOf Type
	err := json.NewDecoder(r.Body).Decode(&typeOf)
	if err!=nil{
		panic(err)
	}
	fmt.Println(typeOf.Name)

	doc:= struct{
		DocType string `json:"docType"`
		Name string `json:"name"`

	}{"Type",typeOf.Name}

	// doc := map[string]interface{}{

		

	// 	"docType": "Type",
	// 	"name":    typeOf.Name,
	// }
	docMap,err:=ToJSONCompatibleMap(doc)
	if err!=nil{
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	fmt.Println(typeOf.Name)

	rev, err := db.Put(context.TODO(),"Type"+typeOf.Name, docMap)
	if err != nil {
		fmt.Println("Error")

		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusCreated, doc)
	fmt.Printf("Type document is inserted with revision %s \n", rev)

	return

}

//updateType creates type
func updateType(w http.ResponseWriter, r *http.Request) {
	fmt.Println("update")

	params:=mux.Vars(r)
	id:=params["id"]

	row:=db.Get(context.TODO(),"Type"+id)
	if row.Rev==""{
	fmt.Println("Error")

		respondJSON(w, http.StatusNotFound,"Incorrect id ("+id+") provided!")
		return
	}
	

	var typeOf Type
	_ = json.NewDecoder(r.Body).Decode(&typeOf)

	var doc map[string]interface{}

	if id=="Type"+ typeOf.Name{
		doc= map[string]interface{}{
			"_rev":row.Rev,
			"doctType": "Type",
			"name": typeOf.Name,

		}

	}else { 
		doc= map[string]interface{}{
			
			"doctType": "Type",
			"name": typeOf.Name,

		}
	}





	fmt.Println(typeOf.Name)
	rev,err := db.Delete(context.TODO(),"Type"+id,row.Rev)
	if err!=nil{
		fmt.Println("del")
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	rev, err = db.Put(context.TODO(),"Type"+typeOf.Name, doc)
	if err != nil {
		fmt.Println("Error")

		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondJSON(w, http.StatusCreated, doc)
	fmt.Printf("Type document is inserted with revision %s \n", rev)

	return

}

//deleteType creates type
func deleteType(w http.ResponseWriter, r *http.Request) {
	fmt.Println("update")

	params:=mux.Vars(r)
	id:=params["id"]

	row:=db.Get(context.TODO(),"Type"+id)
	if row.Rev==""{
	fmt.Println("Error")

		respondJSON(w, http.StatusNotFound,"Incorrect id ("+id+") provided!")
		return
	}
	_,err := db.Delete(context.TODO(),"Type"+id,row.Rev)
	if err!=nil{
		fmt.Println("del")
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusOK,"Sucessfully deleted!")
	return
}
	
//readType creates type
func readType(w http.ResponseWriter, r *http.Request){


	rows,err:=db.Query(context.TODO(), "type", "type",kivik.Options{"include_docs": true})
    if err != nil {
        panic(err)
    }

    fmt.Println("i dont see")
    test:=struct{
        ID string `json:"_id"`
        DocType string `json:"docType"`
        Name string `json:"name"`
    }{}
    results := []interface{}{}
    for rows.Next() {
       
        if err := rows.ScanDoc(&test); err != nil {
            panic(err)
        }
       results=append(results,test)
       fmt.Println(results)
       }
   
    if err!=nil{
        panic(err)
    }

    // doc,_:=ToJSONCompatibleMap(results)
	fmt.Println(results)
	respondJSON(w, http.StatusCreated, results)

}


//createAssetType creates type
func createAssetType(w http.ResponseWriter, r *http.Request) {
	fmt.Println("create")
	
	var element []string
	var astType AssetType
	astType.DocType="AssetType"
	err := json.NewDecoder(r.Body).Decode(&astType)
	if err!=nil{
		panic(err)
	}
	fmt.Println(astType.Name)

	for _,a:= range astType.Attributes{

		_,rev,err:=db.GetMeta(context.TODO(),"Type"+a.Type)
		if err!=nil{
			panic(err)
		}

	if rev==""{
		fmt.Println("append")
	
	element=append(element,a.Type)
	}
}
	
	if len(element)!=0{
		doc:=map[string]interface{}{
			"type": element,
			"status" :"Not found",
		}

		respondJSON(w, http.StatusNotFound,doc)
		return
	}

	


	doc:= struct{
		DocType string `json:"docType"`
		Name string `json:"name"`
		Attributes []Attrs `json:"attributes"`

	}{"Type",astType.Name,astType.Attributes}


	// doc := map[string]interface{}{

		

	// 	"docType": "Type",
	// 	"name":    typeOf.Name,
	// }
	docMap,err:=ToJSONCompatibleMap(doc)
	if err!=nil{
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}


	rev, err := db.Put(context.TODO(),"AssetType"+astType.Name, docMap)
	if err != nil {
		fmt.Println("Error")

		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusCreated, doc)
	fmt.Printf("Type document is inserted with revision %s \n", rev)

	return

}
//updateAssetType creates type
func updateAssetType(w http.ResponseWriter, r *http.Request) {
	fmt.Println("update")

	fmt.Println("update")

	params:=mux.Vars(r)
	id:=params["id"]

	row:=db.Get(context.TODO(),"AssetType"+id)
	if row.Rev==""{
	fmt.Println("Error")

		respondJSON(w, http.StatusNotFound,"Incorrect id ("+id+") provided!")
		return
	}
	
    var element []string
	var astType AssetType
	_ = json.NewDecoder(r.Body).Decode(&astType)

	for _,a:= range astType.Attributes{

		_,rev,err:=db.GetMeta(context.TODO(),"Type"+a.Type)
		if err!=nil{
			panic(err)
		}

	if rev==""{
		fmt.Println("append")
	
	element=append(element,a.Type)
	}
}
	
	if len(element)!=0{
		doc:=map[string]interface{}{
			"type": element,
			"status" :"Not found",
		}

		respondJSON(w, http.StatusNotFound,doc)
		return
	}




	var doc map[string]interface{}

	if id=="AssetType"+ astType.Name{
		doc= map[string]interface{}{
			"_rev":row.Rev,
			"doctType": "AssetType",
			"name": astType.Name,
			"attributes": astType.Attributes,

		}

	}else { 
		doc= map[string]interface{}{
			
			"doctType": "AssetType",
			"name": astType.Name,
			"attributes": astType.Attributes,

		}
	}





	fmt.Println(astType.Name)
	rev,err := db.Delete(context.TODO(),"AssetType"+id,row.Rev)
	if err!=nil{
		fmt.Println("del")
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	rev, err = db.Put(context.TODO(),"Type"+astType.Name, doc)
	if err != nil {
		fmt.Println("Error")

		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}

	respondJSON(w, http.StatusCreated, doc)
	fmt.Printf("Type document is inserted with revision %s \n", rev)

	return
}

//deleteAssetType creates type
func deleteAssetType(w http.ResponseWriter, r *http.Request) {
	fmt.Println("delete")

	params:=mux.Vars(r)
	id:=params["id"]

	row:=db.Get(context.TODO(),"AssetType"+id)
	if row.Rev==""{
	fmt.Println("Error")

		respondJSON(w, http.StatusNotFound,"Incorrect id ("+id+") provided!")
		return
	}
	_,err := db.Delete(context.TODO(),"AssetType"+id,row.Rev)
	if err!=nil{
		fmt.Println("del")
		respondError(w, http.StatusInternalServerError, err.Error())
		return
	}
	respondJSON(w, http.StatusOK,"Sucessfully deleted!")
	return
}
	
//readAssetType creates type
func readAssetType(w http.ResponseWriter, r *http.Request){


	rows,err:=db.Query(context.TODO(), "type", "assetType",kivik.Options{"include_docs": true})
    if err != nil {
        panic(err)
    }

    fmt.Println("i dont see")
    test:=struct{
        ID string `json:"_id"`
        DocType string `json:"docType"`
		Name string `json:"name"`
		Attributes []Attrs `json:"attributes"`
    }{}
    results := []interface{}{}
    for rows.Next() {
       
        if err := rows.ScanDoc(&test); err != nil {
            panic(err)
        }
       results=append(results,test)
       fmt.Println(results)
       }
   
    if err!=nil{
        panic(err)
    }

    // doc,_:=ToJSONCompatibleMap(results)
	fmt.Println(results)
	respondJSON(w, http.StatusCreated, results)

}





// respondJSON makes the response with payload as json format
func respondJSON(w http.ResponseWriter, status int, payload interface{}) {
	response, err := json.Marshal(payload)
	if err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		w.Write([]byte(err.Error()))
		return
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(status)
	w.Write([]byte(response))
}

// respondError makes the error response with payload as json format
func respondError(w http.ResponseWriter, code int, message string) {
	respondJSON(w, code, map[string]string{"error": message})
}
//ToJSONCompatibleMap function
func ToJSONCompatibleMap(obj interface{}) (map[string]interface{},error){
	objAsBytes,err:=json.Marshal(obj)
	if err!=nil{
		return nil,err
	}
	var objAsMap map[string]interface{}
	err=json.Unmarshal(objAsBytes,&objAsMap)
	if err!=nil{
		return nil,err
	}
	return objAsMap,nil
}
